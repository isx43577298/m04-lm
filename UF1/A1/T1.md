Crear fitxers de text
=====================

MP4UF1A1T1

Introducció als fitxers de text: tipus de final de línia, codificació de
caràcters, etc.

Codificació de text
-------------------

Els fitxers XML **són** fitxers de text. Caldrà per tant començar
recordant i ampliant el que ja es coneix sobre aquest tema.

-   Codificacions de caràcters més comunes: ASCII ⊆ ISO-8859-1 (latin1)
    ⊆ Unicode (UTF-8).
-   Caràcters imprimibles (32‥126, 160‥255) i de control (0‥31, 127).
-   Tipus de finals de línia: LF, CR, CR+LF.
-   Contingut de la variable d’entorn LANG (per exemple, `en_US.utf8`).
-   Ordres útils: `dos2unix`(1), `unix2dos`(1), `iconv`(1), `convmv`(1),
    `strings`(1), `od`(1), `hexdump`(1).

La convenció de mencionar ordres del shell en la forma `man`(1) indica
el nom de l’ordre i la secció del manual on està documentada. Per
exemple, si ens interessa el format del fitxer d’usuaris hem d’escriure
`man 5 passwd`.

El shell com intèrpret d’ordres
-------------------------------

-   Metacaràcters: `|  & ; ( ) < >` *espai tabulador*
-   Expansió de paràmetres: `$P`, `${P}`, `$(cmd)`, `$(( expr ))`
-   Escape: `\c`, `'c'`, `"c"`, `'$'`, `\$`
-   Expansió de patrons: `*`, `?`, `[chr]`, `[!chr]`, `[^chr]`
    (no estàndard)
-   Redirecció: `cmd > output`, `cmd < input`, `< input cmd > output`,
    `> fitxer`, `cmd > /dev/null 2>&1`, `cmd 2>&1 | less`
-   Connexió entre processos: `cmd1 | cmd2`

**Eines del shell pel tractament de text**
------------------------------------------

Podem classificar les ordres que podem executar en el shell en dos grans
grups:

-   Ordres per lots (*batch*), **NO** interactives:
    -   `echo`
    -   `cat`, `head`, `tail`, `cut`
    -   `grep`, `egrep`, `fgrep`
-   Ordres visuals, interactives
    -   `less`, `man`, `vim`
    -   `mc`

Enllaços recomanats
-------------------

-   [Wikipedia: Codificación de
    caracteres](http://es.wikipedia.org/wiki/Categor%C3%ADa:Codificaci%C3%B3n_de_caracteres)
-   [Windows code
    pages](https://msdn.microsoft.com/en-us/library/cc195051.aspx)
-   [Drew’s Grep tutorial](http://www.uccs.edu/~ahitchco/grep/)
-   [Introduction to text manipulation on UNIX-based
    systems](http://www.ibm.com/developerworks/aix/library/au-unixtext/)
-   [Otro Tutorial del VI Más](http://www.demiurgo.org/doc/otvim.html)

Pràctiques
----------

-   Consulta les pàgines de manual ascii(7), latin1(7), utf8(7).
-   Compara el resultat de les ordres `man passwd` i `man 5 passwd`.
-   Verifica que entens tots els exemples anteriors de redirecció
    del shell.
-   Converteix un fitxer existent en el sistema a finals de línia propis
    de Windows.
-   Troba cadenes de text dins de fitxers binaris.
-   Converteix la codificació de fitxers de text entre les codificacions
    UTF-8 i latin1.
-   Practica els tutorials citats en la llista d’enllaços.

