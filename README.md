
![Logo](logo.png)


[MP4](MP4.html)
===============

-   [**UF1**: Programació amb XML](MP4.html#MP4UF1AC)
    1.  [XML ben format](MP4.html#acMP4UF1A1)
        -   [Crear fitxers de text](UF1/A1/T1.html){.leaf}
        -   [Editar documents XML ben formats](UF1/A1/T2.html){.leaf}
        -   [Verificar documents ben formats](UF1/A1/T3.html){.leaf}
        -   [Definir i incloure entitats](UF1/A1/T4.html){.leaf}
        -   [Combinar documents XML amb XInclude](UF1/A1/T5.html){.leaf}
        -   [PE1: Conceptes fonamentals dels documents
            XML](UF1/A1/PE1.html){.leaf}
        -   [EP1: Creació de documents XML](UF1/A1/EP1.html){.leaf}

    2.  [XML vàlid](MP4.html#acMP4UF1A2)
        -   [Dissenyar nous DTDs](UF1/A2/T1.html){.leaf}
        -   [Validar documents XML](UF1/A2/T2.html){.leaf}
        -   [Organitzar i documentar DTDs](UF1/A2/T3.html){.leaf}
        -   [PE1: Conceptes bàsics dels DTDs](UF1/A2/PE1.html){.leaf}
        -   [EP1: Creació de nous DTDs](UF1/A2/EP1.html){.leaf}

    3.  [El vocabulari HTML 4](MP4.html#acMP4UF1A3)
        -   [Crear i validar documents XHTML](UF1/A3/T1.html){.leaf}
        -   [Utilitzar documentació interactiva](UF1/A3/T2.html){.leaf}
        -   [Convertir documents HTML a XHTML](UF1/A3/T3.html){.leaf}
        -   [Editar XHTML amb un editor
            estructurat](UF1/A3/T4.html){.leaf}
        -   [Aplicar fulls d’estil a documents
            XHTML](UF1/A3/T5.html){.leaf}
        -   [PE1: Conceptes fonamentals del vocabulari
            XHTML](UF1/A3/PE1.html){.leaf}
        -   [EP1: Creació de documents XHTML
            estricte](UF1/A3/EP1.html){.leaf}
-   [**UF2**: Àmbits d’aplicació de l’XML](MP4.html#MP4UF2AC)
    1.  [La família de vocabularis RSS](MP4.html#acMP4UF2A1)
        -   [Utilitzar lectors i agregadors RSS](UF2/A1/T1.html){.leaf}
        -   [Crear i validar documents RSS](UF2/A1/T2.html){.leaf}
        -   [Aplicar fulls d’estil a documents
            RSS](UF2/A1/T3.html){.leaf}
        -   [PE1: Conceptes fonamentals del vocabulari
            RSS](UF2/A1/PE1.html){.leaf}
        -   [EP1: Creació i validació de documents
            RSS](UF2/A1/EP1.html){.leaf}

    2.  [Sistemes de consulta i emmagatzematge de documents
        XML](MP4.html#acMP4UF2A2)
        -   [Consultar documents XML](UF2/A2/T1.html){.leaf}
        -   [Comprimir documents XML](UF2/A2/T2.html){.leaf}
        -   [Extreure dades de SGBD relacionals en format
            XML](UF2/A2/T3.html){.leaf}
        -   [PE1: Tècniques de programació declarativa amb
            Python](UF2/A2/PE1.html){.leaf}
        -   [EP1: Interrogació de documents XML](UF2/A2/EP1.html){.leaf}

    3.  [Transformació de documents XML](MP4.html#acMP4UF2A3)
        -   [Transformar documents XML](UF2/A3/T1.html){.leaf}
        -   [Organitzar i documentar les
            transformacions](UF2/A3/T2.html){.leaf}
        -   [PE1: Característiques del mòdul ElementTree de
            Python](UF2/A3/PE1.html){.leaf}
        -   [EP1: Programació de
            transformacions](UF2/A3/EP1.html){.leaf}
-   [**UF3**: Sistemes empresarials de gestió
    d’informació](MP4.html#MP4UF3AC)
    1.  [Formularis HTML interactius](MP4.html#acMP4UF3A1)
        -   [Crear formularis](UF3/A1/T1.html){.leaf}
        -   [Presentar formularis i dades utilitzant
            taules](UF3/A1/T2.html){.leaf}
        -   [Processar formularis](UF3/A1/T3.html){.leaf}
        -   [PE1: Conceptes bàsics dels formularis
            HTML](UF3/A1/PE1.html){.leaf}
        -   [EP1: Creació de formularis](UF3/A1/EP1.html){.leaf}
        -   [EP2: Processament de formularis](UF3/A1/EP2.html){.leaf}

------------------------------------------------------------------------
